import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_management/provider/Student_Provider.dart';
import 'HomePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context)=>StudentProvider(),
      child: MaterialApp(
        title: 'Student Management',
        home: HomePage(),
      ),
    );
  }
}
