import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:student_management/provider/Student_Provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {


  @override
  Widget build(BuildContext context) {
    final studentProvider = Provider.of<StudentProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Student Dashboard'),
      ),
      body:
         Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Student Name',
                          fillColor: Colors.white,
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.blue, width: 2.0),
                          ),
                        ),
                        onChanged: (value){
                          studentProvider.changeName(value);
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Student ID',
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue, width: 2.0),
                            ),),
                        onChanged: (value){
                            studentProvider.changeId(value);
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Department',
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue, width: 2.0),
                            ),),
                        onChanged: (value){
                          studentProvider.changeDepartment(value);
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Batch',
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue, width: 2.0),
                            ),),
                        onChanged: (value){
                          studentProvider.changeBatch(value);
                        },
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Current CGPA',
                            fillColor: Colors.white,
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue, width: 2.0),
                            ),),
                        onChanged: (value){
                          studentProvider.changeCgpa(value);
                        },
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Wrap(
                        spacing: 8.0,
                        children: <Widget>[
                          RaisedButton(
                            child: Text('Create'),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.green,
                            onPressed: () {
                              studentProvider.createInfo();
                              print('Your Info created');
                            },
                          ),
                          RaisedButton(
                            child: Text('Read'),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.blue,
                            onPressed: () {
                              studentProvider.readInfo();
                            },
                          ),
                          RaisedButton(
                            child: Text('Update'),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.orange,
                            onPressed: () {},
                          ),
                          RaisedButton(
                            child: Text('Delete'),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            color: Colors.red,
                            onPressed: () {},
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
}
